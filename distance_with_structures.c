//WAP to find the distance between two points using structures and 4 functions.
#include<stdio.h>
#include<math.h>
struct points{
float xa,ya,xb,yb,res;
}p;
int ip()
{
printf("enter the x and y coordinates of point 1: ");
scanf("%f%f",&p.xa,&p.ya);
printf("enter the x and y coordinates of point 2: ");
scanf("%f%f",&p.xb,&p.yb);
}
int calc()
{
p.res = sqrt(pow(p.xa-p.xb,2)+pow(p.ya-p.yb,2));
printf("distance is : %.3lf",p.res);
}
int main()
{
ip();
calc();
}