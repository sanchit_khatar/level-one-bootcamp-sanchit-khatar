#include<stdio.h>
struct fraction{
    int n;
    int d;
};
typedef struct fraction frac;
frac input()
{
    frac f;
    printf("enter the numerator: ");
    scanf("%d",&f.n);
    printf("enter the denominator: ");
    scanf("%d",&f.d);
    return f;
}
int simplified(int x,int y)
{
    int ans;
    for(int i=1;i<=x&&i<=y;i++)
    {
        if(x%i==0&&y%i==0)
        {
            ans = i;
        }
    }
    return ans;
}
frac calc(frac f1,frac f2)
{
    frac total;
    int gcd,a,b;
    a = ((f1.n*f2.d)+(f1.d*f2.n));
    b = (f1.d*f2.d);
    gcd = simplified(a,b);
    total.n = a/gcd;
    total.d = b/gcd;
    return total;
}
void output(frac f1,frac f2,frac total)
{
   printf("the sum of %d/%d + %d/%d = %d/%d",f1.n,f1.d,f2.n,f2.d,total.n,total.d);
}
int main(void)
{
    int num,den,g;
    frac f1,f2,total;
    f1 = input();
    f2 = input();
    total = calc(f1,f2);
    output(f1,f2,total);
    return 0;
}