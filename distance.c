//WAP to find the distance between two point using 4 functions.
#include<stdio.h>
#include<math.h>
float point_1(float *ret_xa,float *ret_ya)
{
float xa,ya;
printf("enter the x coordinate of point 1: ");
scanf("%f",&xa);
printf("enter the y coordinate of point 1: ");
scanf("%f",&ya);
*ret_xa = xa;
*ret_ya = ya;
}
float point_2(float *ret_xb,float *ret_yb)
{
float xb,yb;
printf("enter the x coordinate of point 2: ");
scanf("%f",&xb);
printf("enter the y coordinate of point 2: ");
scanf("%f",&yb);
*ret_xb = xb;
*ret_yb = yb;
}
float calc(float xa,float ya,float xb,float yb )
{
float result;
result = sqrt(pow(xa-xb,2)+pow(ya-yb,2));
return result;
}
int main()
{
float xa,ya,xb,yb,dist;
point_1(&xa,&ya);
point_2(&xb,&yb);
dist = calc(xa,ya,xb,yb);
printf("the distance between two point is: %.3lf",dist);
return 0;
}