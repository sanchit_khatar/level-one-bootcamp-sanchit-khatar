//WAP to find the sum of n fractions.
#include<stdio.h>
struct variables{
   int na,da,gcd,ntemp,dtemp,ntotal,dtotal,num; 
}v;

int simplified()
{
    for(int i = 1;i<=v.ntotal && i<=v.dtotal;i++)
    {
        if(v.ntotal%i == 0 && v.dtotal%i == 0)
        {
            v.gcd = i;
        }
    }
}

int calc()
{
    v.ntemp = v.ntotal;
    v.dtemp = v.dtotal;
    v.ntotal = ((v.ntemp*v.da)+(v.dtemp*v.na));
    v.dtotal = v.dtemp*v.da;
}

int ip()
{
    printf("enter the number of fractions: ");
    scanf("%d",&v.num);
    for(int i = 1; i<=v.num;i++)
    {
        printf("enter the value of numerator of %d num: ",i);
        scanf("%d",&v.na);
        printf("enter the value of denominator of %d num: ",i);
        scanf("%d",&v.da);
        if(i == 1)
        {
            v.ntotal = v.na;
            v.dtotal = v.da;
        }
        else
        {
            calc();
        }
    }
}
int main()
{
    ip();
    printf("the total sum is: %d / %d \n",v.ntotal,v.dtotal);
    simplified();
    printf("the simplified answer is: %d / %d",v.ntotal/v.gcd,v.dtotal/v.gcd);
}